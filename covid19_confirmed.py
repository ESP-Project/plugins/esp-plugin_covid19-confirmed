'''
                                  ESP Health
                         Notifiable Diseases Framework
                            COVID-19 Confirmed Case Generator

@author: Jeff Andre <jandre@commoninf.com>
@organization: Commonwealth Informatics.
@contact: http://esphealth.org
@copyright: (c) 2021 Commonwealth Informatics
@license: LGPL
'''
from ESP.utils import log
from ESP.hef.base import Event
from ESP.hef.base import LabResultPositiveHeuristic
from ESP.nodis.base import DiseaseDefinition

class covid19_confirmed(DiseaseDefinition):
    '''
    covid-19 case definition
    '''
    
    conditions = ['covid19_confirmed']

    uri = 'urn:x-esphealth:disease:commoninf:covid19_confirmed:v1'
    
    short_name = 'covid19_confirmed'
    
    test_name_search_strings = [ 'covid', 'sars', 'cov', 'corona' ]
    
    timespan_heuristics = []
    
    recurrence_interval = 90
    
    @property
    def event_heuristics(self):
        heuristic_list = []
       
        #
        # LabResults for COVID-19
        #
        heuristic_list.append( LabResultPositiveHeuristic(
            test_name = 'covid19_pcr',
             ))

        heuristic_list.append( LabResultPositiveHeuristic(
            test_name = 'covid19_ag',
             ))

        return heuristic_list

    def generate(self):
        log.info('Generating cases of %s' % self.short_name) 
        #
        # Criteria #1 - Confirmed COVID19
        # Positive PCR test
        lx_pcr_event_names = [
                'lx:covid19_pcr:positive',
                ]
        lx_pcr_event_qs = Event.objects.filter(
                            name__in = lx_pcr_event_names,
                )
        #
        # Criteria #2 - Probable COVID19
        # Positive Antigen test
        lx_antigen_event_names = [
                'lx:covid19_ag:positive',
                ]
        lx_antigen_event_qs = Event.objects.filter(
                            name__in = lx_antigen_event_names,
                )
        # All events
        all_event_qs = lx_pcr_event_qs | lx_antigen_event_qs
        all_event_qs = all_event_qs.exclude(
                    case__condition=self.conditions[0],
                ).order_by('date')
        # create cases
        counter = 0
        for this_event in all_event_qs:
            case_criteria = ''
            if this_event.name in lx_pcr_event_names:
                case_criteria = 'Confirmed case - Positive SARS-CoV2 PCR test result'
            elif this_event.name in lx_antigen_event_names:
                case_criteria = 'Probable case - Positive SARS-CoV2 Antigen test result and no positive SARS-CoV2 PCR result'
            if case_criteria:
                created, this_case = self._create_case_from_event_obj(
                    condition=self.conditions[0],
                    criteria=case_criteria,
                    recurrence_interval=self.recurrence_interval,
                    event_obj=this_event,
                    relevant_event_qs=None
                )
                if created:
                    log.debug('Created new COVID19 case: %s' % this_case)
                    counter += 1

        log.debug('Generating %s new cases of COVID19' % counter)

        return counter

    def report_field(self, report_field, case):
        reportable_fields = {
            'na_trmt_obx': False,
            'symptom_obx': False,
            'NA-56': 'NA-1739',
            '10187-3': 'NA-1738',
        }

        return reportable_fields.get(report_field, None)

#-------------------------------------------------------------------------------
#
# Packaging
#
#-------------------------------------------------------------------------------

covid19_confirmed_definition = covid19_confirmed()

def event_heuristics():
    return covid19_confirmed_definition.event_heuristics

def disease_definitions():
    return [covid19_confirmed_definition]
