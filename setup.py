'''
                                  ESP Health
                          COVID-19 Confirmed Disease Definition
                             Packaging Information
                                  
@author: Jeff Andre <jandre@commoninf.com>
@organization: Commonwealth Informatics.
@contact: http://esphealth.org
@copyright: (c) 2021 Commonwealth Informatics
@license: LGPL 3.0 - http://www.gnu.org/licenses/lgpl-3.0.txt
'''

from setuptools import setup
from setuptools import find_packages

setup(
    name = 'esp-plugin-covid19-confirmed',
    version = '1.2',
    author = 'Jeff Andre',
    author_email = 'jandre@commoninf.com',
    description = 'COVID-19 confirmed definition module for ESP Health application',
    license = 'LGPLv3',
    keywords = 'COVID-19 confirmed algorithm disease surveillance public health epidemiology',
    url = 'http://esphealth.org',
    packages = find_packages(exclude=['ez_setup']),
    install_requires = [
        ],
    entry_points = '''
        [esphealth]
        disease_definitions = covid19_confirmed:disease_definitions
        event_heuristics = covid19_confirmed:event_heuristics
    '''
    )
